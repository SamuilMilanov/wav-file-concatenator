#ifndef WRITE_DATA_H
#define WRITE_DATA_H


void write_wav(FILE *fp_out, union header_data *header_bytes, short *data, int data_size);

void write_data(FILE *fp_out, union header_data *header_bytes, short *data, char *file_name, int silent_flag);

void write_silence(FILE *fp_out, char *data, int number_of_zeros);
#endif