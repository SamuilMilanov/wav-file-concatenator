#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "wav_types.h"
//including getopt.c to get the implementation of optarg and optopt 
#include "getopt.c"
#include "read_data_and_header.h"
#include "get_size.h"
#include "write_data.h"
#include "helper_func.h"

#define MAX_SIZE_PATH 256
#define MAX_NEW_NAME_SIZE 1024

int main(int argc, char *argv[]){
    // Get the options from the arguments
    int opt;
    int silent_flag = 0;
    char *value_opt = NULL;
    while((opt = getopt(argc, argv, "sn:")) != -1) 
    { 
        switch(opt) 
        { 
            case 's': 
                silent_flag = 1;
            case 'n': 
                value_opt = optarg; 
            case ':': 
                
                break; 
            case '?': 
                printf("unknown option: %c\n", optopt);
                break; 
        } 
    } 

    //Getting the paths for both files
    char path_1[MAX_SIZE_PATH];
    char path_2[MAX_SIZE_PATH];
    char path_3[MAX_SIZE_PATH];
    char position[10];

    FILE *fp1;
    FILE *fp2;

    if(!silent_flag){
        printf("Enter first path: ");
    }
    gets(path_1);
    if(!silent_flag){
        printf("Enter second path: ");
    }
    gets(path_2);  

    //Opening files and checking if path's are correct
    fp1 = fopen(path_1, "rb");
    if (fp1 == NULL){
        printf("Incorrect first file path!");
        exit(1);
    }

    fp2 = fopen(path_2, "rb");
    if (fp2 == NULL){
        printf("Incorrect second file path!");
        exit(1);
    }

    if(!silent_flag){
        printf("Please enter file and position: ");
    }
    gets(path_3);
    gets(position);
    int  order;
    order = get_order(path_1, path_2, path_3, position);
    char new_name[MAX_NEW_NAME_SIZE];
    
    get_new_name(path_1, path_2, new_name, order);
    

    if (!silent_flag){
        get_size(fp1, fp2, path_1, path_2, value_opt);
    }
    
    
    union header_data *header_1;
    header_1 = (union header_data *) malloc(sizeof(union header_data));

    union header_data *header_2;
    header_2 = (union header_data *) malloc(sizeof(union header_data));

    read_header(fp2, header_2, path_2);  
    read_header(fp1, header_1, path_1);

    short* data_1;
    short* data_2;
    data_1 = read_data(fp1, header_1);
    data_2 = read_data(fp2, header_2);

    //Creating the data for the 5 seconds of silence
    char* silence;
    int number_zeros = 44100 * 2 * 2 * 5;  //The sampling rate of audio is 44100 multiplied by the  
    silence = (char*) malloc(number_zeros);//the number of bytes per sample * number of channels * seconds
    
    //Adjusting the new header to match the new file, by using one of the old header and adjusting the values of the chunk_size and subchunk size
    int old_data_size = get_file_old_size(order, header_1, header_2);
    
    // Creating the new file and writing all the information in it
    FILE* new_file;
    new_file = fopen(new_name, "wb");
    if(new_file == NULL){
        fprintf(stderr, "Error: Couldn't open output file\n");
        exit(1);
    }
    if(order == 1){
        write_wav(new_file, header_1, data_1, old_data_size);
        write_silence(new_file, silence, number_zeros);
        write_data(new_file, header_2, data_2, new_name, silent_flag);
    }
    else if (order == 2){
        write_wav(new_file, header_2, data_2, old_data_size);
        write_silence(new_file, silence, number_zeros);
        write_data(new_file, header_1, data_1, new_name, silent_flag);
    }


    free(header_1);
    free(header_2);
    free(data_1);
    free(data_2);
    free(silence);
    fclose(fp1);
    fclose(fp2);
    fclose(new_file);
    return 0;
}