#ifndef HELPER_FUNC_H
#define HELPER_FINC_H

int get_order(char* path_1, char* path_2, char* path_3, char* pos);

void get_new_name(char* path_1, char* path_2, char* new_name, int order);

int get_file_old_size(int order, union header_data *header_1, union header_data *header_2);

#endif