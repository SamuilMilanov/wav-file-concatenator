#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "wav_types.h"

//Function that figures out if the first path is going to be concatenated front or back 
// returns 1 if the first path is going first
// returns 2 if the second path is going to be first
int get_order(char* path_1, char* path_2, char* path_3, char* pos){
    if((strcmp(path_1, path_3) == 0) && (strcmp(pos, "front") == 0)){
        return 1;
    }
    else if((strcmp(path_2, path_3) == 0) && (strcmp(pos, "back") == 0)){
        return 1;
    }
    else{
        return 2;
    }
}

void get_new_name(char* path_1, char* path_2, char* new_name, int order){
    char new_path_1[256];
    for(int i = strlen(path_1) - 1; i >= 0; i--){
        if(path_1[i] == '\\'){
            i++;
            int p = 0;
            for(p = 0; p < strlen(path_1) - 4 - i; p++){
                new_path_1[p] = path_1[i + p];
            }
            new_path_1[p] = '\0';
            break;
        }
    }
    char new_path_2[256];
    for(int i = strlen(path_2) - 1; i >= 0; i--){
        if(path_2[i] == '\\'){
            i++;
            int p = 0;
            for(p = 0; p < strlen(path_2) - 4 - i; p++){
                new_path_2[p] = path_2[i + p];
            }
            new_path_2[p] = '\0';

            break;
        }
    }
    

    if(order == 1){
        int i = 0;
        for(i = 0; i < strlen(new_path_1) - 1; i++){
            new_name[i] = new_path_1[i];
        }
        int p = 0;
        for(p = 0; p < strlen(new_path_2)- 1; p++){
            new_name[i + p] = new_path_2[p];
        }
        
        strcat(new_name, "_concatenated.wav");
    }
    else{
        int i = 0;
        for(i = 0; i < strlen(new_path_2); i++){
            new_name[i] = new_path_2[i];
        }
        int p = 0;
        for(p = 0; p < strlen(new_path_1); p++){
            new_name[i + p] = new_path_1[p];
        }
        strcat(new_name, "_concatenated.wav");
    }
    
}

int get_file_old_size(int order, union header_data *header_1, union header_data *header_2){
    int old_data_size;
    int number_zeros = 44100 * 2 * 2 * 5;
    if(order == 1){
        old_data_size = header_1->header.subchunk2_size.int_value;
        header_1->header.chunk_size.int_value += header_2->header.subchunk2_size.int_value;
        header_1->header.chunk_size.int_value += number_zeros*2;
        header_1->header.subchunk2_size.int_value = header_1->header.chunk_size.int_value - 44;
    }
    else if (order == 2){
        old_data_size = header_2->header.subchunk2_size.int_value;
        header_2->header.chunk_size.int_value += header_1->header.subchunk2_size.int_value;
        header_2->header.chunk_size.int_value += number_zeros*2;
        header_2->header.subchunk2_size.int_value = header_2->header.chunk_size.int_value - 44;
    }
    return old_data_size;
}