#ifndef READ_DATA_AND_HEADER_H
#define READ_DATA_AND_HEADER_H

short *read_data(FILE *fp_in, union header_data *header_bytes);

void read_header(FILE *fp, union header_data *file_bytes, char *file_name);

#endif