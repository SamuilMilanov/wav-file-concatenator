#include <stdlib.h>
#include <stdio.h>
#include "wav_types.h"


// function that writes the header of the new file and the data of the first file
void write_wav(FILE *fp_out, union header_data *header_bytes, short *data, int data_size) {
  fwrite(header_bytes, sizeof(union header_data), 1, fp_out);
  

  fwrite(data, sizeof(short), data_size / 2, fp_out);
}

// function that writes just the data of a file, without the header
void write_data(FILE *fp_out, union header_data *header_bytes, short *data, char *file_name, int silent_flag) {
  if(!silent_flag){
    printf("Writing to file... ");
  }

  int data_size = header_bytes->header.chunk_size.int_value - 36;
  
  fwrite(data, sizeof(short), data_size / 2, fp_out);

  if(!silent_flag){
    printf(" done.\n");
  }
}

// function that writes the silence into the file
void write_silence(FILE *fp_out, char *data, int number_of_zeros){
  fwrite(data, sizeof(char), number_of_zeros, fp_out);
} 