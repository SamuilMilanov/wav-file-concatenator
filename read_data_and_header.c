#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "wav_types.h"

//Function that reads the header and saves it into the struct 
void read_header(FILE *fp, union header_data *file_bytes, char *file_name) {
  int i;
  for (i = 0; i < HEADER_SIZE && (file_bytes->data[i] = getc(fp)) != EOF; i++);

  if (i != HEADER_SIZE) {
    fprintf(stderr, "Error: file %s header data incomplete\n", file_name);
    exit(3);
  }
  
}

//Function that reads the data and saves it
short *read_data(FILE *fp_in, union header_data *header_bytes){
  short *data;
  short bits_per_sample = header_bytes->header.bits_per_sample.short_value;
  int data_size = header_bytes->header.chunk_size.int_value - 36;

  // checking if bits per sample are 16
  if (bits_per_sample != 16) {
    fprintf(stderr, "Error: Samples are not 16-bits. Can't process.\n");
    exit(2);
  }

  data = (short *) malloc(data_size);
  // checking if memory is allocated
  if (data == NULL) {
    fprintf(stderr, "Error: Couldn't allocate memory for samples\n");
    exit(3);
  }

  short *dp = data;
  fread(dp, sizeof(short), data_size / 2, fp_in);
  
  return data; 
}





