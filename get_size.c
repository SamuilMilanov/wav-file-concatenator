#include <stdlib.h>
#include <stdio.h>
#include <string.h>
void get_size(FILE *fp1, FILE *fp2, char *path_1, char *path_2, char* count_system){
    fseek(fp1, 0L, SEEK_END);
    int sz1 = ftell(fp1);
    rewind(fp1);

    fseek(fp2, 0L, SEEK_END);
    int sz2 = ftell(fp2);
    rewind(fp2);

    if (sz1 > sz2 && (count_system == NULL || !(strcmp(count_system, "dec")))){
        printf("The sound content of %s has %d more bytes sound data than %s\n", path_1, sz1 - sz2, path_2);
    }
    else if (count_system == NULL || !(strcmp(count_system, "dec"))){
        printf("The sound content of %s has %d more bytes sound data than %s\n", path_2, sz2 - sz1, path_1);
    }
    else if(sz1 > sz2 && !(strcmp(count_system, "hex"))){
        printf("The sound content of %s has %X more bytes sound data than %s\n", path_1, sz1 - sz2, path_2);
    }
    else if(sz1 < sz2 && !(strcmp(count_system, "hex"))){
        printf("The sound content of %s has %X more bytes sound data than %s\n", path_2, sz2 - sz1, path_1);
    }
    else if(sz1 > sz2 && !(strcmp(count_system, "oct"))){
        printf("The sound content of %s has %o more bytes sound data than %s\n", path_1, sz1 - sz2, path_2);
    }
    else if(sz1 < sz2 && !(strcmp(count_system, "oct"))){
        printf("The sound content of %s has %o more bytes sound data than %s\n", path_2, sz2 - sz1, path_1);
    }
    rewind(fp1);
    rewind(fp2);
}